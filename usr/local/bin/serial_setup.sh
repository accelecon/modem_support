#!/bin/sh

exec 2> /dev/console
[ -f /flash/debug.all ] && set -xv
[ -f /flash/debug.x.all ] && set -x
[ -f /flash/debug.serial_setup ] && set -xv
[ -f /flash/debug.x.serial_setup ] && set -x

# Parse the results of the value passed from udev to determine what device is in use
# we can only really handle the "add" case here,  but an add shows up as a
# move/change or other things. Play it safe,  iff ACTION is remove exit

SERIAL=${SERIAL:-$id}
SERIAL_TYPE=${SERIAL_TYPE:-$1}
SERIAL_MAKER=${SERIAL_MAKER:-$2}
SERIAL_MODEL=${SERIAL_MODEL:-$3}
SERIAL_serialno=

# ensure only one instance of this script runs for any one device, previously
# up to 4 or more instances would run simultaneously and it just isn't safe
# or predictable

touch /tmp/serial_setup_pid.$$
locked=
mutex -c serial_setup.$SERIAL && locked=1
try=0
while [ -z "$locked" -a $try -lt 30 ]; do
  try=$((try + 1))
  sleep 5
  mutex -c serial_setup.$SERIAL && locked=1
done
if [ -z "$locked" ];  then
  log "Cannot acquire serial_setup.$SERIAL mutex."
  rm -f /tmp/serial_setup_pid.$$
  exit 1
fi
trap "mutex -r serial_setup.$SERIAL; rm -f /tmp/serial_setup_pid.$$" 0


[ "$ACTION" = "remove" ] && exit 0

#
# probe for the devices serial number so that in the future we can support
# more than one of these devices on a hub and correctly number the ports
# even if they are probed in a different order. We would do this we would
# store the serial no and the "base" port number for it in /flash.
#

probe_serial_number()
{
  REPORT_FILE=/tmp/$$-report.out
  (
    stty 115200 raw
	chat -r $REPORT_FILE -t 3 \
	  'REPORT' 'Serial' \
	  '' 'logout' '>-logout->-logout->' 'login' \
	  'name:' 'admin' \
	  'word:' 'admin' \
	  '>' 'ver' \
	  '>'
  ) < $DEVNAME > $DEVNAME
  SERIAL_serialno=$(sed -n 's/^.*Serial.*: \([^ ]*\).*$/\1/p' < $REPORT_FILE)
  rm -f $REPORT_FILE
}

# helper for writing the serial env.  We only want to write out settings if
# they do not exist,  or,  we have probed the serial and potentially changed
# settings
#
write_serial_info()
{
  # if we have no value, do nothing and return
  [ -z "$2" ] && return
  # only save setting if it was probed, or,  we have not set it previously
  [ -z "$SERIAL_PROBED" -a -s "/var/run/serials/$SERIAL/$1" ] && return
  # all good,  save the setting
  echo "$2" > "/var/run/serials/$SERIAL/$1"
}

build_serial_env() {
  mkdir -p /var/run/serials/$SERIAL

  write_serial_info SERIAL_TYPE     "$SERIAL_TYPE"
  write_serial_info SERIAL_VID      "$ID_VENDOR_ID"
  write_serial_info SERIAL_PID      "$ID_MODEL_ID"
  write_serial_info SERIAL_MAKER    "$SERIAL_MAKER"
  write_serial_info SERIAL_MODEL    "$SERIAL_MODEL"
  write_serial_info SERIAL_DEV      "$DEVNAME"
  write_serial_info SERIAL_serialno "$SERIAL_serialno"
}

probe_serial_number
build_serial_env

#quit if we've already setup this device
mutex -a serial.list.$MODEM || exit

echo "$SERIAL" >> /tmp/serial.list
