#!/bin/sh -x
# This is expected to be sourced so that it can see and change the environment.

disambiguate(){
  # Since at&t Force modem, t-mobile Jet, and Telus E372 have same Vid/Pid, determine which one is in use
  if [ "$ID_MODEL_ID" == '1506' ]; then
    model=`scmd i "$PORT" | fgrep '[Model:' | cut -f2 -d ' ' | cut -f1 -d ']'`
    if [ "$model" == 'E368' ]; then
      MODEM_TYPE='hw368'
      MODEM_MAKER='HW'
      MODEM_MODEL='E368'
      MODEM_DESC='Huawei E368'
      PORT_ORDER="scmd=0,pppd=1"
      reassign=yes
      MODEM_PROBED=yes
    fi
    if [ "$model" == 'E372' ]; then
      MODEM_TYPE='hw372'
      MODEM_MAKER='HW'
      MODEM_MODEL='E372'
      MODEM_DESC='Huawei E372'
      MODEM_PROBED=yes
    fi
    if [ "$model" == 'E1815' ]; then
      MODEM_TYPE='hw1815'
      MODEM_MAKER='HW'
      MODEM_MODEL='E1815'
      MODEM_DESC='Huawei E1815'
      MODEM_PROBED=yes
    fi
    if [ "$model" == 'K4605' ]; then
      MODEM_TYPE='hw4605'
      MODEM_MAKER='HW'
      MODEM_MODEL='K4605'
      MODEM_DESC='Huawei K4605'
      pppd_dev=$MODEM_SCMD_DEV
      scmd_dev=$MODEM_PPPD_DEV
      MODEM_PPPD_DEV=$pppd_dev
      MODEM_SCMD_DEV=$scmd_dev
      MODEM_PROBED=yes
    fi
    if [ "$model" == 'E397B' ]; then
      MODEM_TYPE='hw397'
      MODEM_MAKER='HW'
      MODEM_MODEL='E397B'
      MODEM_DESC='Huawei E397B'
      scmd_no=$(echo $MODEM_SCMD_DEV | tr -d '/dev/ttyUSB')
      pppd_no=$((scmd_no+1))
      MODEM_PPPD_DEV="/dev/ttyUSB${pppd_no}"
      MODEM_PROBED=yes
    fi
  fi

  # Sierra Wireless disambiguation
  if [ "$ID_MODEL_ID" == '68a3' ] || [ "$ID_MODEL_ID" == '68aa' ]; then
    ati_resp=$(scmd i "$PORT" | grep -i model)
    case "$ati_resp" in
      *305*)
        MODEM_TYPE='sw305'
        MODEM_MAKER='Sierra Wireless'
        MODEM_MODEL='USB305'
        MODEM_DESC='Sierra Wireless USB305'
        MODEM_PROBED=yes
      ;;
      *AC326U*)
        MODEM_TYPE='sw326'
        MODEM_MAKER='Sierra Wireless'
        MODEM_MODEL='AC326U'
        MODEM_DESC='Sierra Wireless AC326U'
        MODEM_PROBED=yes
      ;;
      *319*)
        MODEM_TYPE='sw319'
        MODEM_MAKER='Sierra Wireless'
        MODEM_MODEL='USB319U'
        MODEM_DESC='Sierra Wireless USB319U'
        # 319U modem is still in testing. Based on the info provided by AT&T, we know USB2 is available,
        # but we need to see if USB3 is available.  If not, then we need to configure a different port
        pppd_dev=$MODEM_SCMD_DEV
        scmd_dev=$MODEM_PPPD_DEV
        MODEM_PPPD_DEV=$pppd_dev
        MODEM_SCMD_DEV=$scmd_dev
        MODEM_PROBED=yes
      ;;
      *330U*)
        MODEM_TYPE='sw330'
        MODEM_MAKER='Sierra Wireless'
        MODEM_MODEL='USB330U'
        MODEM_DESC='Sierra Wireless USB330U'
        MODEM_PROBED=yes
      ;;
    esac
  fi
}

disambiguate
