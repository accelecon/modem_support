#!/bin/sh

exec 2> /dev/console
[ -f /flash/debug.all ] && set -xv
[ -f /flash/debug.x.all ] && set -x
[ -f /flash/debug.modem_setup ] && set -xv
[ -f /flash/debug.x.modem_setup ] && set -x

# Parse the results of the value passed from udev to determine what modem is in use
# we can only really handle the "add" case here,  but an add shows up as a
# move/change or other things. Play it safe,  iff ACTION is remove exit

MODEM=${MODEM:-$id}
MODEM_TYPE=${MODEM_TYPE:-$1}
MODEM_MAKER=${MODEM_MAKER:-$2}
MODEM_MODEL=${MODEM_MODEL:-$3}
MODEM_FAMILY=${MODEM_FAMILY:-$4}
MODEM_DIAL=${MODEM_DIAL:-$5}
PORT_ORDER=${PORT_ORDER:-$6}
SORTING_METHOD=${SORTING_METHOD:-$7}
MODEM_PROBED=

# ensure only one instance of this script runs for any one modem, previously
# up to 4 or more instances would run simultaneously and it just isn't safe
# or predictable

touch /tmp/modem_setup_pid.$$
locked=
mutex -c modem_setup.$MODEM && locked=1
try=0
while [ -z "$locked" -a $try -lt 30 ]; do
  try=$((try + 1))
  sleep 5
  mutex -c modem_setup.$MODEM && locked=1
done
if [ -z "$locked" ];  then
  log "Cannot acquire modem_setup.$MODEM mutex."
  rm -f /tmp/modem_setup_pid.$$
  exit 1
fi
trap "mutex -r modem_setup.$MODEM; rm -f /tmp/modem_setup_pid.$$" 0


if [ "$ACTION" = "remove" ]; then
	if [ "$ID_VENDOR_ID" -a "$ID_MODEL_ID" ]; then
		# if the device was a registered in use modem then signal it
		# as a disconnect
		if grep "$ID_VENDOR_ID" /var/run/modems/*/MODEM_VID > /dev/null 2>&1 && grep "$ID_MODEL_ID" /var/run/modems/*/MODEM_PID > /dev/null 2>&1; then
			touch /tmp/flag.modem_disconnect
		fi
	fi
	exit 0
fi

# special case for the admin interface when the sw341 is in ippassthough
# mode,  we do not want to use this interface,  just remember it for later
if [ "$ID_VENDOR_ID" = "1199" -a "$ID_MODEL_ID" = "9057" ]; then
	case "$INTERFACE" in
	usb*)
		mkdir -p /var/run/modems/$MODEM
		echo "$INTERFACE" > /var/run/modems/$MODEM/admin_dev
		exit 0
		;;
	esac
fi

#
# the Huawei/Vodafone K3773-M2M looks just like this modem only it has an
# NCM interface rather than a QMI interface. Implement the smarts here to
# change us to a different modem type.
if [ "$MODEM_TYPE" = "hw366" ]; then
	USB_BUS=$(expr ${MODEM} : '\([0-9]*\)-.*$')
	USB_PORT=$(expr ${MODEM} : '.*-\([0-9]*\).*$')
	INTFS=$(cat /sys/bus/usb/devices/usb$USB_BUS/$USB_BUS-$USB_PORT/bNumInterfaces 2> /dev/null)
	if [ "${INTFS:-0}" -eq 4 ]; then
		MODEM_TYPE="k3773-m2m"
		MODEM_MODEL="K3773-M2M"
		MODEM_FAMILY="gsm3"
		PORT_ORDER="scmd=0,pppd=0"
	fi
fi

#
# The Beam is notorious on the FX for inducing EMI? errors and disconnects,
# we can work around this by dropping to full speed as a last resort. We do
# this on a per boot basis to allow for the device to work better when shipped
# to a different location without EMI?/disconnect errors.
#
# Since we have seen these errors on other modems, implement it for
# everything so that this problem is fixed for any situation is affects us.
# To make it Beam specific again add the following to the usb_adjust check:
#           "$ID_VENDOR_ID" = "1199" -a "$ID_MODEL_ID" = "9051"

usb_adjust=$(cat /flash/cfg.usb_adjust 2> /dev/null)
if [ -z "$usb_adjust" -o "${usb_adjust:-0}" -ne 0 ]; then
	USB_BUS=$(expr ${MODEM} : '\([0-9]*\)-.*$')
	USB_PORT=$(expr ${MODEM} : '.*-\([0-9]*\).*$')
	SPEED=$(cat /sys/bus/usb/devices/usb$USB_BUS/$USB_BUS-$USB_PORT/speed 2> /dev/null)
	if [ "${SPEED:-0}" -gt 12 ]; then
		EMI=$(dmesg | fgrep 'EMI?' | wc -l)
		DISCON=$(dmesg | fgrep 'USB disconnect' | wc -l)
		if [ "${EMI:-0}" -gt 0 -o "${DISCON:-0}" -gt 2 ]; then
			msg="switching to USB full-speed mode (${EMI:-0}:${DISCON:-0})"
			echo "$msg" > /flash/value.restart_msg
			syslog status "restart $msg"
			mutex -a modem_list.$MODEM
			echo 1 > /flash/cfg.usb_adjust_runtime
			sleep 2
			reboot -f
			exit 0
		fi
	else
		# next boot we try hi speed, ugly but it works if we move the unit
		# to a different location.
		# /dev/null only needed because of rabid reentrancy to this script,
		# which is another problem that needs to be addressed
		rm -f /flash/cfg.usb_adjust_runtime 2> /dev/null
	fi
fi

#
# helper for writing the modem env.  We only want to write out settings if
# they do not exist,  or,  we have probed the modem and potentially changed
# settings
#
write_modem_info()
{
  # if we have no value, do nothing and return
  [ -z "$2" ] && return
  # only save setting if it was probed, or,  we have not set it previously
  [ -z "$MODEM_PROBED" -a -s "/var/run/modems/$MODEM/$1" ] && return
  # all good,  save the setting
  echo "$2" > "/var/run/modems/$MODEM/$1"
}

build_modem_env() {
  mkdir -p /var/run/modems/$MODEM

  write_modem_info MODEM_TYPE     "$MODEM_TYPE"
  write_modem_info MODEM_VID      "$ID_VENDOR_ID"
  write_modem_info MODEM_PID      "$ID_MODEL_ID"
  write_modem_info MODEM_MAKER    "$MODEM_MAKER"
  write_modem_info MODEM_MODEL    "$MODEM_MODEL"
  write_modem_info DIAL           "$MODEM_DIAL"
  write_modem_info net_dev        "$INTERFACE"
  write_modem_info MODEM_FAMILY   "$MODEM_FAMILY"

  write_modem_info MODEM_SCMD_DEV "$MODEM_SCMD_DEV"
  write_modem_info MODEM_PPPD_DEV "$MODEM_PPPD_DEV"

  # make sure we use the probed versions and not the version we have now
  dev=$(cat /var/run/modems/$MODEM/MODEM_SCMD_DEV 2> /dev/null)
  [ "$dev" ] && ln -fs "$dev" /var/run/modems/$MODEM/scmd_dev
  dev=$(cat /var/run/modems/$MODEM/MODEM_PPPD_DEV 2> /dev/null)
  [ "$dev" ] && ln -fs "$dev" /var/run/modems/$MODEM/pppd_dev

  [ "${DEVNAME}" != "${DEVNAME%%cdc-wdm*}" ] && ln -s "$DEVNAME" "/var/run/modems/$MODEM/cdc-wdm"
  [ "${DEVNAME}" ] && ln -fs "$DEVNAME" "/var/run/modems/$MODEM/${DEVNAME##*/}"

  #setup family
  family_list=$MODEM_FAMILY
  OLDIFS=$IFS
  IFS=,
  for family in $family_list; do
    touch /tmp/flag.family_$family.$MODEM
    touch /tmp/flag.family_$(echo $family | tr -d "[0-9]").$MODEM
  done
  IFS=$OLDIFS
}

assign_ports() {
  OLDIFS=$IFS
  IFS=,
  for assignment in $PORT_ORDER; do
    #an assignment looks like "pppd=2"; see 10-modem.rules
    port_type=${assignment%%=*}
    port_type=$(echo $port_type | tr '[:lower:]' '[:upper:]' )
    port_num=${assignment##*=}
    if [ "$SORTING_METHOD" = 'devpath' ]; then
      #"port_num" means "nth tty port on this device", so find all tty ports on this device, choose the nth port to transform the name to /dev/ttyUSBm, and save that name
      eval MODEM_${port_type}_DEV=$(find /sys$DEVPATH/../../.. -type d -name "ttyUSB*" -maxdepth 1 -exec basename {} + |\
                                    sed -n $((port_num+1))s_^_/dev/_p)
    elif [ "$SORTING_METHOD" = 'usbiface' -o "$SORTING_METHOD" = '' ]; then
      #"port_num" means "nth interface on this device - thaty may be ttyUSBn or it may be ttyUSBn+m if there are other modems already
      [ "$port_num" -eq "$ID_USB_INTERFACE_NUM" ] && eval MODEM_${port_type}_DEV="$DEVNAME"
    fi
  done
  IFS=$OLDIFS
}

process_modem() {
  assign_ports

  port_order=$PORT_ORDER
  if [ "$SUBSYSTEM" = 'tty' ]; then
    # only one of the PORT vars will be set per instance of this script,  just
    # make sure to only run modem_probe.sh once as it flipflops some settings
    # for some modems like the K4605 and the sw319 and we don't want
    # to double swap them.
    PORT="$MODEM_SCMD_DEV"
    [ "$PORT" ] || PORT="$MODEM_PPPD_DEV"
    [ "$PORT" ] && . /usr/local/bin/modem_probe.sh
  fi
  [ $reassign ] && assign_ports

  # Before we actually put files in the FS see if we have a Sprint 341u
  # we force it into ippassthrough mode and ignore this modem,  it will
  # reappear with a different ID,  similarly for turning it off
  ippassthrough=$(cat /flash/cfg.ippassthrough 2> /dev/null)
  if [ "$ID_VENDOR_ID" = "1199" -a "$ID_MODEL_ID" = "9055" -a ${ippassthrough:-1} -ne 0 ]; then
    scmd '!ippassthrough=1' "$MODEM_SCMD_DEV"
    exit 0
  fi

  if [ "$ID_VENDOR_ID" = "1199" -a "$ID_MODEL_ID" = "9057" -a ${ippassthrough:-1} -eq 0 ]; then
    scmd '!ippassthrough=0' "$MODEM_SCMD_DEV"
    exit 0
  fi

  build_modem_env
}

# re-use pinger to restart the connection quickly on disconnect
# These can come in fast so try not to miss any
modem_reconnect() {
	rm -f /tmp/flag.modem_disconnect
	echo 0 > /tmp/value.pingtime
	/nb/pinger.sh
	sleep 10
	while [ -f /tmp/flag.modem_disconnect ]
	do
		rm -f /tmp/flag.modem_disconnect
		echo 0 > /tmp/value.pingtime
		/nb/pinger.sh
		sleep 10
	done
	mutex -r modem_disconnect
}

process_modem

# look for a disconected event and try to recover quickly
if [ -f /tmp/flag.modem_disconnect ] && mutex -a modem_disconnect; then
	# try to only have one pinger going at a time and not miss any
	# disconnects
	modem_reconnect &
fi

#quit if we've already done this modem
mutex -a modem.list.$MODEM || exit

echo "$MODEM" >> /tmp/modem.list
